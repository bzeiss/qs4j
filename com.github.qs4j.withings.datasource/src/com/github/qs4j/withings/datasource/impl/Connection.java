package com.github.qs4j.withings.datasource.impl;

import java.util.Properties;

import org.eclipse.datatools.connectivity.oda.IConnection;
import org.eclipse.datatools.connectivity.oda.IDataSetMetaData;
import org.eclipse.datatools.connectivity.oda.IQuery;
import org.eclipse.datatools.connectivity.oda.OdaException;

import com.github.qs4j.withings.IWithings;
import com.github.qs4j.withings.WithingsException;
import com.github.qs4j.withings.WithingsFactory;
import com.github.qs4j.withings.model.WithingsUser;
import com.ibm.icu.util.ULocale;

public class Connection implements IConnection {
	private boolean m_isOpen = false;
	private Properties connProperties;

	// -----------------------------------------------------------------------------

	public void open(Properties connProperties) throws OdaException {
		this.connProperties = connProperties;
		validateConnection();

		m_isOpen = true;
	}

	// -----------------------------------------------------------------------------
	
	private void validateConnection() throws OdaException {
		IWithings withings = WithingsFactory.getInstance();
		withings.setConsumerToken(connProperties.getProperty("com.github.qs4j.withings.datasource.consumerToken"), connProperties.getProperty("com.github.qs4j.withings.datasource.consumerTokenSecret"));
		withings.addAccessToken(connProperties.getProperty("com.github.qs4j.withings.datasource.accessToken"), connProperties.getProperty("com.github.qs4j.withings.datasource.accessTokenSecret"), Integer.parseInt(connProperties.getProperty("com.github.qs4j.withings.datasource.userId")));
		try {
			WithingsUser userInformation = withings.getUserInformation(Integer.parseInt(connProperties.getProperty("com.github.qs4j.withings.datasource.userId")));
			if ((userInformation != null) && (userInformation.getId() != Integer.parseInt(connProperties.getProperty("com.github.qs4j.withings.datasource.userId"))))
					throw new OdaException("Invalid user ID!");
					
		} catch (NumberFormatException e1) {
			throw new OdaException("User ID is not a number!");
		} catch (WithingsException e1) {
			throw new OdaException(e1.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public void setAppContext(Object context) throws OdaException {
	}

	// -----------------------------------------------------------------------------

	public void close() throws OdaException {
		m_isOpen = false;
	}

	// -----------------------------------------------------------------------------

	public boolean isOpen() throws OdaException {
		return m_isOpen;
	}

	// -----------------------------------------------------------------------------

	public IDataSetMetaData getMetaData(String dataSetType) throws OdaException {
		return new DataSetMetaData(this);
	}

	// -----------------------------------------------------------------------------

	public IQuery newQuery(String dataSetType) throws OdaException {
		return new Query(connProperties);
	}

	// -----------------------------------------------------------------------------

	public int getMaxQueries() throws OdaException {
		return 0;
	}

	// -----------------------------------------------------------------------------

	public void commit() throws OdaException {
	}

	// -----------------------------------------------------------------------------

	public void rollback() throws OdaException {
	}

	// -----------------------------------------------------------------------------

	public void setLocale(ULocale locale) throws OdaException {
	}

	// -----------------------------------------------------------------------------

}
