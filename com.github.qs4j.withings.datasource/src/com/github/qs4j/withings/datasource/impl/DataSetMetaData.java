package com.github.qs4j.withings.datasource.impl;

import org.eclipse.datatools.connectivity.oda.IConnection;
import org.eclipse.datatools.connectivity.oda.IDataSetMetaData;
import org.eclipse.datatools.connectivity.oda.IResultSet;
import org.eclipse.datatools.connectivity.oda.OdaException;

public class DataSetMetaData implements IDataSetMetaData {
	private IConnection m_connection;

	// -----------------------------------------------------------------------------

	DataSetMetaData(IConnection connection) {
		m_connection = connection;
	}

	// -----------------------------------------------------------------------------

	public IConnection getConnection() throws OdaException {
		return m_connection;
	}

	// -----------------------------------------------------------------------------

	public IResultSet getDataSourceObjects(String catalog, String schema,
			String object, String version) throws OdaException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public int getDataSourceMajorVersion() throws OdaException {
		// TODO Auto-generated method stub
		return 1;
	}

	// -----------------------------------------------------------------------------

	public int getDataSourceMinorVersion() throws OdaException {
		// TODO Auto-generated method stub
		return 0;
	}

	// -----------------------------------------------------------------------------

	public String getDataSourceProductName() throws OdaException {
		// TODO Auto-generated method stub
		return "qs4j Withings Data Source Data Source";
	}

	// -----------------------------------------------------------------------------

	public String getDataSourceProductVersion() throws OdaException {
		return Integer.toString(getDataSourceMajorVersion()) + "." + //$NON-NLS-1$
				Integer.toString(getDataSourceMinorVersion());
	}

	// -----------------------------------------------------------------------------

	public int getSQLStateType() throws OdaException {
		// TODO Auto-generated method stub
		return IDataSetMetaData.sqlStateSQL99;
	}

	// -----------------------------------------------------------------------------

	public boolean supportsMultipleResultSets() throws OdaException {
		return false;
	}

	// -----------------------------------------------------------------------------

	public boolean supportsMultipleOpenResults() throws OdaException {
		return false;
	}

	// -----------------------------------------------------------------------------

	public boolean supportsNamedResultSets() throws OdaException {
		return false;
	}

	// -----------------------------------------------------------------------------

	public boolean supportsNamedParameters() throws OdaException {
		// TODO Auto-generated method stub
		return false;
	}

	// -----------------------------------------------------------------------------

	public boolean supportsInParameters() throws OdaException {
		// TODO Auto-generated method stub
		return true;
	}

	// -----------------------------------------------------------------------------

	public boolean supportsOutParameters() throws OdaException {
		// TODO Auto-generated method stub
		return false;
	}

	// -----------------------------------------------------------------------------

	public int getSortMode() {
		// TODO Auto-generated method stub
		return IDataSetMetaData.sortModeNone;
	}

	// -----------------------------------------------------------------------------

}
