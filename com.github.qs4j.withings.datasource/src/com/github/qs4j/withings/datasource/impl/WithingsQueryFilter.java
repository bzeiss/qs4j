package com.github.qs4j.withings.datasource.impl;

import java.io.Serializable;
import java.util.Date;

import com.github.qs4j.withings.model.enums.Category;
import com.github.qs4j.withings.model.enums.DeviceType;
import com.github.qs4j.withings.model.enums.MeasureType;

public class WithingsQueryFilter implements Serializable {
	private static final long serialVersionUID = -8481905026280382477L;
	private int xmlSchemaVersion = 1;
	private Boolean startDateEnabled = false;
	private Boolean endDateEnabled = false;
	private Boolean updateAfterDateEnabled = false;
	private Boolean categoryEnabled = true;
	private Boolean measureTypeEnabled = false;
	private Boolean deviceTypeEnabled = true;
	private Boolean limitEnabled = false;
	private Boolean offsetEnabled = false;
	private Date startDate = new Date();
	private Date endDate = new Date();
	private Date updatedAfterDate = new Date();
	private Category category = Category.Measure;
	private DeviceType deviceType = DeviceType.BodyScale;
	private MeasureType measureType = MeasureType.WeightKg;
	private Integer limit = 100;
	private Integer offset = 0;

	public MeasureType getMeasureType() {
		return measureType;
	}

	public void setMeasureType(MeasureType measureType) {
		this.measureType = measureType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Date getUpdatedAfterDate() {
		return updatedAfterDate;
	}

	public Category getCategory() {
		return category;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public Integer getLimit() {
		return limit;
	}

	public Date getStartDateNulled() {
		if (getStartDateEnabled() == false)
			return null;
		return startDate;
	}

	public Date getEndDateNulled() {
		if (getEndDateEnabled() == false)
			return null;
		return endDate;
	}

	public Date getUpdatedAfterDateNulled() {
		if (getUpdateAfterDateEnabled() == false)
			return null;
		return updatedAfterDate;
	}

	public Category getCategoryNulled() {
		if (getCategoryEnabled() == false)
			return null;
		return category;
	}

	public DeviceType getDeviceTypeNulled() {
		if (getDeviceTypeEnabled() == false)
			return null;
		return deviceType;
	}

	public Integer getLimitNulled() {
		if (getLimitEnabled() == false)
			return null;

		return limit;
	}

	public Integer getOffsetNulled() {
		if (getOffsetEnabled() == false)
			return null;

		return offset;
	}

	public MeasureType getMeasureTypeNulled() {
		if (getMeasureTypeEnabled() == false)
			return null;

		return measureType;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setUpdatedAfterDate(Date updatedAfterDate) {
		this.updatedAfterDate = updatedAfterDate;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Boolean getStartDateEnabled() {
		return startDateEnabled;
	}

	public void setStartDateEnabled(Boolean startDateEnabled) {
		this.startDateEnabled = startDateEnabled;
	}

	public Boolean getEndDateEnabled() {
		return endDateEnabled;
	}

	public void setEndDateEnabled(Boolean endDateEnabled) {
		this.endDateEnabled = endDateEnabled;
	}

	public Boolean getUpdateAfterDateEnabled() {
		return updateAfterDateEnabled;
	}

	public void setUpdateAfterDateEnabled(Boolean updateAfterDateEnabled) {
		this.updateAfterDateEnabled = updateAfterDateEnabled;
	}

	public Boolean getCategoryEnabled() {
		return categoryEnabled;
	}

	public void setCategoryEnabled(Boolean categoryEnabled) {
		this.categoryEnabled = categoryEnabled;
	}

	public Boolean getDeviceTypeEnabled() {
		return deviceTypeEnabled;
	}

	public void setDeviceTypeEnabled(Boolean deviceTypeEnabled) {
		this.deviceTypeEnabled = deviceTypeEnabled;
	}

	public Boolean getLimitEnabled() {
		return limitEnabled;
	}

	public void setLimitEnabled(Boolean limitEnabled) {
		this.limitEnabled = limitEnabled;
	}

	public Boolean getOffsetEnabled() {
		return offsetEnabled;
	}

	public void setOffsetEnabled(Boolean offsetEnabled) {
		this.offsetEnabled = offsetEnabled;
	}

	public int getFilterVersion() {
		return xmlSchemaVersion;
	}

	public void setFilterVersion(int filterVersion) {
		this.xmlSchemaVersion = filterVersion;
	}

	public Boolean getMeasureTypeEnabled() {
		return measureTypeEnabled;
	}

	public void setMeasureTypeEnabled(Boolean measureTypeEnabled) {
		this.measureTypeEnabled = measureTypeEnabled;
	}

}
