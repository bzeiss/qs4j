package com.github.qs4j.withings.datasource.impl;

import org.eclipse.datatools.connectivity.oda.IParameterMetaData;
import org.eclipse.datatools.connectivity.oda.OdaException;

public class ParameterMetaData implements IParameterMetaData {

	// -----------------------------------------------------------------------------

	public int getParameterCount() throws OdaException {
		return 1;
	}

	// -----------------------------------------------------------------------------

	public int getParameterMode(int param) throws OdaException {
		return IParameterMetaData.parameterModeIn;
	}

	// -----------------------------------------------------------------------------

	public String getParameterName(int param) throws OdaException {
		return null;
	}

	// -----------------------------------------------------------------------------

	public int getParameterType(int param) throws OdaException {
		return java.sql.Types.CHAR; // as defined in data set extension manifest
	}

	// -----------------------------------------------------------------------------

	public String getParameterTypeName(int param) throws OdaException {
		int nativeTypeCode = getParameterType(param);
		return Driver.getNativeDataTypeName(nativeTypeCode);
	}

	// -----------------------------------------------------------------------------

	public int getPrecision(int param) throws OdaException {
		return -1;
	}

	// -----------------------------------------------------------------------------

	public int getScale(int param) throws OdaException {
		return -1;
	}

	// -----------------------------------------------------------------------------

	public int isNullable(int param) throws OdaException {
		return IParameterMetaData.parameterNullableUnknown;
	}

	// -----------------------------------------------------------------------------

}
