package com.github.qs4j.withings.datasource.impl;

import org.eclipse.datatools.connectivity.oda.IConnection;
import org.eclipse.datatools.connectivity.oda.IDriver;
import org.eclipse.datatools.connectivity.oda.LogConfiguration;
import org.eclipse.datatools.connectivity.oda.OdaException;
import org.eclipse.datatools.connectivity.oda.util.manifest.DataTypeMapping;
import org.eclipse.datatools.connectivity.oda.util.manifest.ExtensionManifest;
import org.eclipse.datatools.connectivity.oda.util.manifest.ManifestExplorer;

public class Driver implements IDriver {
	static String ODA_DATA_SOURCE_ID = "com.github.qs4j.withings.datasource"; //$NON-NLS-1$

	// -----------------------------------------------------------------------------

	public IConnection getConnection(String dataSourceType) throws OdaException {
		// assumes that this driver supports only one type of data source,
		// ignores the specified dataSourceType
		return new Connection();
	}

	// -----------------------------------------------------------------------------

	public void setLogConfiguration(LogConfiguration logConfig)
			throws OdaException {
		// do nothing; assumes simple driver has no logging
	}

	// -----------------------------------------------------------------------------

	public int getMaxConnections() throws OdaException {
		return 1; // we don't want to stress the withings servers
	}

	// -----------------------------------------------------------------------------

	public void setAppContext(Object context) throws OdaException {
		// do nothing; assumes no support for pass-through context
	}

	// -----------------------------------------------------------------------------

	static ExtensionManifest getManifest() throws OdaException {
		return ManifestExplorer.getInstance().getExtensionManifest(
				ODA_DATA_SOURCE_ID);
	}

	// -----------------------------------------------------------------------------

	static String getNativeDataTypeName(int nativeDataTypeCode)
			throws OdaException {
		DataTypeMapping typeMapping = getManifest().getDataSetType(null)
				.getDataTypeMapping(nativeDataTypeCode);
		if (typeMapping != null)
			return typeMapping.getNativeType();
		return "Non-defined";
	}

}
