package com.github.qs4j.withings.datasource.impl;

import java.util.ArrayList;

import org.eclipse.datatools.connectivity.oda.IResultSetMetaData;
import org.eclipse.datatools.connectivity.oda.OdaException;

public class ResultSetMetaData implements IResultSetMetaData {
	private ArrayList<String> columns = null;
	private ArrayList<Integer> columnTypes = null;
	private int measureStartIndex;

	// -----------------------------------------------------------------------------

	public ResultSetMetaData(ArrayList<String> columns, ArrayList<Integer> columnTypes, int measureStartIndex) {
		this.columns = columns;
		this.columnTypes = columnTypes;
		this.measureStartIndex = measureStartIndex;
	}

	// -----------------------------------------------------------------------------

	public int getColumnCount() throws OdaException {
		if (columns == null)
			return 0;
		
		return columns.size();
	}

	// -----------------------------------------------------------------------------

	public String getColumnName(int index) throws OdaException {
		if (index-1 < columns.size())
			return columns.get(index-1);
		
		return "Unknown";
	}

	// -----------------------------------------------------------------------------

	public String getColumnLabel(int index) throws OdaException {
		return getColumnName(index);
	}

	// -----------------------------------------------------------------------------

	public int getColumnType(int index) throws OdaException {
		return columnTypes.get(index-1);
	}

	// -----------------------------------------------------------------------------

	public String getColumnTypeName(int index) throws OdaException {
		int nativeTypeCode = getColumnType(index);
		return Driver.getNativeDataTypeName(nativeTypeCode);
	}

	// -----------------------------------------------------------------------------

	public int getColumnDisplayLength(int index) throws OdaException {
		return 8;
	}

	// -----------------------------------------------------------------------------

	public int getPrecision(int index) throws OdaException {
		return -1;
	}

	// -----------------------------------------------------------------------------

	public int getScale(int index) throws OdaException {
		return -1;
	}

	// -----------------------------------------------------------------------------

	public int isNullable(int index) throws OdaException {
		return IResultSetMetaData.columnNullableUnknown;
	}

	// -----------------------------------------------------------------------------

	public ArrayList<String> getColumns() {
		return columns;
	}

	// -----------------------------------------------------------------------------

	public ArrayList<Integer> getColumnTypes() {
		return columnTypes;
	}

	// -----------------------------------------------------------------------------

	public int getMeasureStartIndex() {
		return measureStartIndex;
	}

	// -----------------------------------------------------------------------------

	public void setMeasureStartIndex(int measureStartIndex) {
		this.measureStartIndex = measureStartIndex;
	}

	// -----------------------------------------------------------------------------
	
}
