package com.github.qs4j.withings.datasource.impl;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.datatools.connectivity.oda.IBlob;
import org.eclipse.datatools.connectivity.oda.IClob;
import org.eclipse.datatools.connectivity.oda.IResultSet;
import org.eclipse.datatools.connectivity.oda.IResultSetMetaData;
import org.eclipse.datatools.connectivity.oda.OdaException;

import com.github.qs4j.withings.model.WithingsMeasure;
import com.github.qs4j.withings.model.WithingsMeasureGroup;
import com.github.qs4j.withings.model.WithingsMeasures;
import com.github.qs4j.withings.model.enums.MeasureType;

public class ResultSet implements IResultSet {
	@SuppressWarnings("unused")
	private int m_maxRows;
	private int m_currentRowId = -1;
	private WithingsMeasures measures;
	private ResultSetMetaData resultSetMetaData = null;
	private HashMap<String, Integer> nextSpareMatch = new HashMap<String, Integer>();
	
	// -----------------------------------------------------------------------------

	public ResultSet(WithingsMeasures measures,
			ResultSetMetaData resultSetMetaData) {
		this.measures = measures;
		this.resultSetMetaData = resultSetMetaData;
	}

	// -----------------------------------------------------------------------------

	public IResultSetMetaData getMetaData() throws OdaException {
		return this.resultSetMetaData;
	}

	// -----------------------------------------------------------------------------

	public void setMaxRows(int max) throws OdaException {
		m_maxRows = max;
	}

	// -----------------------------------------------------------------------------

	protected int getMaxRows() {
		return measures.getMeasureGroups().size();
	}

	// -----------------------------------------------------------------------------

	public boolean next() throws OdaException {
		int maxRows = getMaxRows();

		if (m_currentRowId < maxRows - 1) {
			m_currentRowId++;
			return true;
		}

		return false;
	}

	// -----------------------------------------------------------------------------

	public void close() throws OdaException {
		m_currentRowId = -1;
	}

	// -----------------------------------------------------------------------------

	public int getRow() throws OdaException {
		return m_currentRowId;
	}

	// -----------------------------------------------------------------------------

	public String getString(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			Object fieldValue = field.get(measureGroup);
			if (fieldValue == null)
				return ""; // we may have actually null values here (e.g.
							// for comments), so we return empty strings for
							// those)
			String str = (String) (fieldValue.toString());
			if (str == null)
				return ""; // we may have actually null values here (e.g.
							// for comments), so we return empty strings for
							// those)
			return new String(nes(str));
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public String getString(String columnName) throws OdaException {
		return getString(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public int getInt(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			if (field.getType().getName().contains("Integer")) {
				Integer value = (Integer) field.get(measureGroup);
				if (value == null)
					throw new UnsupportedOperationException("value is null");
				return (new Integer(nes(value))).intValue();
			} else if (field.getType().getName().contains("Long")) {
				Long value = (Long) field.get(measureGroup);
				if (value == null)
					throw new UnsupportedOperationException("value is null");
				return (new Long(nes(value))).intValue();
			}
			throw new UnsupportedOperationException("cannot be converted");
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public int getInt(String columnName) throws OdaException {
		return getInt(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public double getDouble(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		if (index < resultSetMetaData.getMeasureStartIndex()) {
			try {
				Field field = measureGroup.getClass().getDeclaredField(
						resultSetMetaData.getColumns().get(index - 1));
				field.setAccessible(true);
				Double value = (Double) field.get(measureGroup);
				if (value == null)
					throw new UnsupportedOperationException("value is null");
				return new Double(nes(value));
			} catch (NoSuchFieldException e) {
				throw new UnsupportedOperationException(e.getLocalizedMessage());
			} catch (SecurityException e) {
				throw new UnsupportedOperationException(e.getLocalizedMessage());
			} catch (IllegalArgumentException e) {
				throw new UnsupportedOperationException(e.getLocalizedMessage());
			} catch (IllegalAccessException e) {
				throw new UnsupportedOperationException(e.getLocalizedMessage());
			}
		} else {
			String columnName = resultSetMetaData.getColumns().get(index - 1);
			Iterator<WithingsMeasure> it = measureGroup.getMeasures().iterator();
			while (it.hasNext()) {
				WithingsMeasure measure = it.next();
				MeasureType type = measure.getType();
				
				if (type.name().equals(columnName)) {
					return measure.getRealValue();
				} 
			}
			// find value for spare entries by forward scanning and caching the next index as result
			int currentRow = m_currentRowId+1;
			Integer nextSpareMatchIndex = nextSpareMatch.get(columnName);
			if ((nextSpareMatchIndex != null) && (nextSpareMatchIndex > m_currentRowId)) {
				currentRow = nextSpareMatchIndex;
			} else {
				nextSpareMatch.remove(columnName);
			}

			for (int i=currentRow; i < getMaxRows(); i++) {
				WithingsMeasureGroup mg = measures.getMeasureGroups().get(
						i);
				String cn = resultSetMetaData.getColumns().get(index - 1);
				Iterator<WithingsMeasure> it2 = mg.getMeasures().iterator();
				while (it2.hasNext()) {
					WithingsMeasure measure = it2.next();
					MeasureType type = measure.getType();
					
					if (type.name().equals(cn)) {
						nextSpareMatch.put(cn, i);
						return measure.getRealValue();
					} 
				}
			}
			
			return -1.0;
		}
	}

	// -----------------------------------------------------------------------------

	public double getDouble(String columnName) throws OdaException {
		return getDouble(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public BigDecimal getBigDecimal(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			if (field.getType().getName().contains("Integer")) {
				Integer value = (Integer) field.get(measureGroup);
				if (value == null)
					throw new UnsupportedOperationException("value is null");
				return new BigDecimal(nes(value));
			} else if (field.getType().getName().contains("Long")) {
				Long value = (Long) field.get(measureGroup);
				if (value == null)
					throw new UnsupportedOperationException("value is null");
				return new BigDecimal(nes(value));
			}
			throw new UnsupportedOperationException("cannot be converted");
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public BigDecimal getBigDecimal(String columnName) throws OdaException {
		return getBigDecimal(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public Date getDate(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			java.util.Date date = (java.util.Date) field.get(measureGroup);
			if (date == null)
				throw new UnsupportedOperationException("date is null");
			return new java.sql.Date(nes(date.getTime()));
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public Date getDate(String columnName) throws OdaException {
		return getDate(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public Time getTime(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			java.util.Date date = (java.util.Date) field.get(measureGroup);
			if (date == null)
				throw new UnsupportedOperationException("date is null");
			return new Time(nes(date.getTime()));
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public Time getTime(String columnName) throws OdaException {
		return getTime(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public Timestamp getTimestamp(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			java.util.Date date = (java.util.Date) field.get(measureGroup);
			if (date == null)
				throw new UnsupportedOperationException("date is null");
			return new Timestamp(nes(date.getTime()));
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public Timestamp getTimestamp(String columnName) throws OdaException {
		return getTimestamp(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public IBlob getBlob(int index) throws OdaException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public IBlob getBlob(String columnName) throws OdaException {
		return getBlob(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public IClob getClob(int index) throws OdaException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public IClob getClob(String columnName) throws OdaException {
		return getClob(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public boolean getBoolean(int index) throws OdaException {
		WithingsMeasureGroup measureGroup = measures.getMeasureGroups().get(
				m_currentRowId);

		try {
			Field field = measureGroup.getClass().getDeclaredField(
					resultSetMetaData.getColumns().get(index - 1));
			field.setAccessible(true);
			Boolean value = (Boolean) field.get(measureGroup);
			if (value == null)
				throw new UnsupportedOperationException("value is null");
			return nes(new Boolean(value));
		} catch (NoSuchFieldException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (SecurityException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalArgumentException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			throw new UnsupportedOperationException(e.getLocalizedMessage());
		}
	}

	// -----------------------------------------------------------------------------

	public boolean getBoolean(String columnName) throws OdaException {
		return getBoolean(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public Object getObject(int index) throws OdaException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public Object getObject(String columnName) throws OdaException {
		return getObject(findColumn(columnName));
	}

	// -----------------------------------------------------------------------------

	public boolean wasNull() throws OdaException {
		// hard-coded for demo purpose
		return false;
	}

	// -----------------------------------------------------------------------------

	public int findColumn(String columnName) throws OdaException {
		// slow, but I don' think this gets called in our scenario
		
		for (int i=0; i < resultSetMetaData.getColumns().size(); i++) {
			String name = resultSetMetaData.getColumns().get(i);
			if (name.equals(columnName))
				return i+1;
		}
		return 1;
	}

	// -----------------------------------------------------------------------------

	private String nes(String value) {
		if (value == null)
			return "";
		return value;
	}

	// -----------------------------------------------------------------------------

	private Integer nes(Integer value) {
		if (value == null)
			return new Integer(0);
		return value;
	}

	// -----------------------------------------------------------------------------

	private Long nes(Long value) {
		if (value == null)
			return new Long(0);
		return value;
	}

	// -----------------------------------------------------------------------------

	private Double nes(Double value) {
		if (value == null)
			return new Double(0.0);
		return value;
	}

	// -----------------------------------------------------------------------------

	private Boolean nes(Boolean value) {
		if (value == null)
			return new Boolean(true);
		return value;
	}

}
