package com.github.qs4j.withings.datasource.impl;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;

import org.eclipse.datatools.connectivity.oda.IParameterMetaData;
import org.eclipse.datatools.connectivity.oda.IQuery;
import org.eclipse.datatools.connectivity.oda.IResultSet;
import org.eclipse.datatools.connectivity.oda.IResultSetMetaData;
import org.eclipse.datatools.connectivity.oda.OdaException;
import org.eclipse.datatools.connectivity.oda.SortSpec;
import org.eclipse.datatools.connectivity.oda.spec.QuerySpecification;

import com.github.qs4j.withings.IWithings;
import com.github.qs4j.withings.WithingsException;
import com.github.qs4j.withings.WithingsFactory;
import com.github.qs4j.withings.model.WithingsMeasure;
import com.github.qs4j.withings.model.WithingsMeasureGroup;
import com.github.qs4j.withings.model.WithingsMeasures;
import com.github.qs4j.withings.model.enums.MeasureType;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class Query implements IQuery {
	@SuppressWarnings("unused")
	private int m_maxRows;
	private String m_preparedText;
	private Properties connProperties;
	private WithingsQueryFilter withingsQueryFilter = new WithingsQueryFilter();
	private WithingsMeasures measures = null;

	private ArrayList<String> columns = null;
	private ArrayList<Integer> columnTypes = null;
	private ResultSetMetaData resultSetMetaData = null;
	private int measureStartIndex = -1;
	
	public Query(Properties connProperties) throws OdaException {
		this.connProperties = connProperties;
	}

	// -----------------------------------------------------------------------------

	public void prepare(String queryText) throws OdaException {
		// prepare filter from queryText
		XStream xstream = new XStream(new StaxDriver()); 
		
		withingsQueryFilter = (WithingsQueryFilter) xstream.fromXML(queryText);
		
		fetchMeasures();
		
		// build necessary column information for resultsetmetadata
		if ((this.columns == null) || (this.columnTypes == null)) {
			buildColumns();
		}
	}

	// -----------------------------------------------------------------------------

	public void setAppContext(Object context) throws OdaException {
	}

	// -----------------------------------------------------------------------------

	public void close() throws OdaException {
		m_preparedText = null;
	}

	// -----------------------------------------------------------------------------

	public IResultSetMetaData getMetaData() throws OdaException {
		if (this.resultSetMetaData == null)
			this.resultSetMetaData = new ResultSetMetaData(columns, columnTypes, measureStartIndex); 
		
		return this.resultSetMetaData;
	}

	// -----------------------------------------------------------------------------

	private void fetchMeasures() throws OdaException {
		IWithings withings = WithingsFactory.getInstance();
		withings.setConsumerToken(
				connProperties
						.getProperty("com.github.qs4j.withings.datasource.consumerToken"),
				connProperties
						.getProperty("com.github.qs4j.withings.datasource.consumerTokenSecret"));
		withings.addAccessToken(
				connProperties
						.getProperty("com.github.qs4j.withings.datasource.accessToken"),
				connProperties
						.getProperty("com.github.qs4j.withings.datasource.accessTokenSecret"),
				Integer.parseInt(connProperties
						.getProperty("com.github.qs4j.withings.datasource.userId")));
		try {
			Integer userId = Integer.parseInt(connProperties
					.getProperty("com.github.qs4j.withings.datasource.userId"));
			
			if (withingsQueryFilter != null) {
				measures = withings.getMeasures(
						userId, 
						withingsQueryFilter.getStartDateNulled(), 
						withingsQueryFilter.getEndDateNulled(), 
						withingsQueryFilter.getDeviceTypeNulled(), 
						withingsQueryFilter.getMeasureTypeNulled(), 
						withingsQueryFilter.getUpdatedAfterDateNulled(), 
						withingsQueryFilter.getCategoryNulled(),
						withingsQueryFilter.getLimitNulled(), 
						withingsQueryFilter.getOffsetNulled());
			} else {
				measures = withings.getMeasures(
						userId, null, null, null, MeasureType.WeightKg, null, null,
						null, null);
			}
		} catch (WithingsException e) {
			throw new OdaException(e.getLocalizedMessage());
		}

	}
	
	// -----------------------------------------------------------------------------

	public IResultSet executeQuery() throws OdaException {
			IResultSet resultSet = new ResultSet(measures, resultSetMetaData);
			setMaxRows(getMaxRows());
			resultSet.setMaxRows(getMaxRows());

			return resultSet;
	}

	// -----------------------------------------------------------------------------

	public void setProperty(String name, String value) throws OdaException {
		System.out.println("[setProperty] name: " + name + " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setMaxRows(int max) throws OdaException {
		m_maxRows = max;
	}

	// -----------------------------------------------------------------------------

	public int getMaxRows() throws OdaException {
		return measures.getMeasureGroups().size();
	}

	// -----------------------------------------------------------------------------

	public void clearInParameters() throws OdaException {
	}

	// -----------------------------------------------------------------------------

	public void setInt(String parameterName, int value) throws OdaException {
		System.out.println("[setInt] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setInt(int parameterId, int value) throws OdaException {
		System.out.println("[setInt] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setDouble(String parameterName, double value)
			throws OdaException {
		System.out.println("[setDouble] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setDouble(int parameterId, double value) throws OdaException {
		System.out.println("[setDouble] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setBigDecimal(String parameterName, BigDecimal value)
			throws OdaException {
		System.out.println("[setBigDecimal] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setBigDecimal(int parameterId, BigDecimal value)
			throws OdaException {
		System.out.println("[setBigDecimal] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setString(String parameterName, String value)
			throws OdaException {
		System.out.println("[setString] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setString(int parameterId, String value) throws OdaException {
		System.out.println("[setString] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setDate(String parameterName, Date value) throws OdaException {
		System.out.println("[setDate] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setDate(int parameterId, Date value) throws OdaException {
		System.out.println("[setDate] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setTime(String parameterName, Time value) throws OdaException {
		System.out.println("[setTime] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setTime(int parameterId, Time value) throws OdaException {
		System.out.println("[setTime] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setTimestamp(String parameterName, Timestamp value)
			throws OdaException {
		System.out.println("[setTimestamp] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setTimestamp(int parameterId, Timestamp value)
			throws OdaException {
		System.out.println("[setTimestamp] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setBoolean(String parameterName, boolean value)
			throws OdaException {
		System.out.println("[setBoolean] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setBoolean(int parameterId, boolean value) throws OdaException {
		System.out.println("[setBoolean] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setObject(String parameterName, Object value)
			throws OdaException {
		System.out.println("[setObject] parameterName: " + parameterName
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setObject(int parameterId, Object value) throws OdaException {
		System.out.println("[setObject] parameterId: " + parameterId
				+ " - value: " + value);
	}

	// -----------------------------------------------------------------------------

	public void setNull(String parameterName) throws OdaException {
	}

	// -----------------------------------------------------------------------------

	public void setNull(int parameterId) throws OdaException {
		System.out.println("[setNull] parameterId: " + parameterId);
	}

	// -----------------------------------------------------------------------------

	public int findInParameter(String parameterName) throws OdaException {
		return 0;
	}

	// -----------------------------------------------------------------------------

	public IParameterMetaData getParameterMetaData() throws OdaException {
		return new ParameterMetaData();
	}

	// -----------------------------------------------------------------------------

	public void setSortSpec(SortSpec sortBy) throws OdaException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public SortSpec getSortSpec() throws OdaException {
		return null;
	}

	// -----------------------------------------------------------------------------

	public void setSpecification(QuerySpecification querySpec)
			throws OdaException, UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public QuerySpecification getSpecification() {
		return null;
	}

	// -----------------------------------------------------------------------------

	public String getEffectiveQueryText() {
		return m_preparedText;
	}

	// -----------------------------------------------------------------------------

	public void cancel() throws OdaException, UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	// -----------------------------------------------------------------------------

	public WithingsQueryFilter getWithingsQueryFilter() {
		return withingsQueryFilter;
	}

	// -----------------------------------------------------------------------------

	public void setWithingsQueryFilter(WithingsQueryFilter filter) {
		this.withingsQueryFilter = filter;
	}

	// -----------------------------------------------------------------------------

	private void buildColumns() {
		columns = new ArrayList<String>();
		columnTypes = new ArrayList<Integer>();
		
		// build columns from measure group
		Field[] fields = WithingsMeasureGroup.class.getDeclaredFields();
		for (int i=0; i < fields.length; i++) {
			Field field = fields[i];
			Class<?> type = field.getType();
			if (!field.getName().equals("measures") && !field.getName().equals("serialVersionUID")) {
				columns.add(field.getName());
				columnTypes.add(mapColumnType(type));
			}
		}
		HashSet<MeasureType> foundMeasureTypes = new HashSet<MeasureType>();
		measureStartIndex = columns.size();
		
		// build columns from measures in measure groups
		Iterator<WithingsMeasureGroup> mgIt = measures.getMeasureGroups().iterator();
		while (mgIt.hasNext()) {
			WithingsMeasureGroup measureGroup = mgIt.next();
			Iterator<WithingsMeasure> it = measureGroup.getMeasures().iterator();
			while (it.hasNext()) {
				WithingsMeasure measure = it.next();
				if (!foundMeasureTypes.contains(measure.getType())) {
					foundMeasureTypes.add(measure.getType());
					columns.add(measure.getType().name());
					columnTypes.add(java.sql.Types.DOUBLE);
				}
			}
		}
	}

	// -----------------------------------------------------------------------------

	private Integer mapColumnType(Class<?> type) {
		if (type.getName().equals("java.lang.Long")) {
			return java.sql.Types.INTEGER;
		} else if (type.getName().equals("java.lang.Double")) {
			return java.sql.Types.DOUBLE;
		} else if (type.getName().equals("java.util.Date")) {
			return java.sql.Types.TIMESTAMP;
		} else if (type.getName().equals("java.lang.String")) {
			return java.sql.Types.VARCHAR;
		} else if (type.getName().equals("java.lang.Boolean")) {
			return java.sql.Types.BOOLEAN;
		} else if (type.getName().equals("com.github.qs4j.withings.model.enums.AttributionStatus")) {
			return java.sql.Types.VARCHAR;
		} else if (type.getName().equals("com.github.qs4j.withings.model.enums.Category")) {
			return java.sql.Types.VARCHAR;
		} else if (type.getName().equals("com.github.qs4j.withings.model.enums.DeviceType")) {
			return java.sql.Types.VARCHAR;
		} else if (type.getName().equals("com.github.qs4j.withings.model.enums.MeasureType")) {
			return java.sql.Types.VARCHAR;
		}
		return java.sql.Types.VARCHAR;
	}

}
