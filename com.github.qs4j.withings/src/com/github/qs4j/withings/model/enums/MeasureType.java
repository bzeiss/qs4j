package com.github.qs4j.withings.model.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum MeasureType implements Serializable {
	WeightKg(1),
	HeightMeter(4),
	FatFreeMassKg(5),
	FatRatioPercent(6),
	FatMassWeightKg(8),
	DiastolicBloodPressureMmhg(9),
	SystolicBloodPressureMmhg(10),
	HeartPulseBpm(11);

	private int id;
	
	private MeasureType(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static MeasureType fromId(int id) {
		for (MeasureType e : values())
			if (e.getId() == id)
				return e;
		return null;
	}
	
};