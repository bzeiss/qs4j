package com.github.qs4j.withings.model.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AttributionStatus implements Serializable {
	MeasureGroupCapturedByKnownUnsharedDevice(0),
	MeasureGroupCapturedByKnownSharedDevice(1),	
	MeasureGroupManuallyEnteredByUser(2),
	MeasureGroupManuallyEnteredByUserDuringUserCreation(4);

	private int id;

	private AttributionStatus(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static AttributionStatus fromId(int id) {
		for (AttributionStatus e : values())
			if (e.getId() == id)
				return e;
		return null;
	}
	
};