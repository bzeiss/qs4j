package com.github.qs4j.withings.model.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender implements Serializable {
	Male(0), 
	Female(1);

	private int id;

	private Gender(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static Gender fromId(int id) {
		for (Gender e : values())
			if (e.getId() == id)
				return e;
		return null;
	}

};