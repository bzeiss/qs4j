package com.github.qs4j.withings.model.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FatMethod implements Serializable {
	Version2009(0), 
	Version2010(1),
	AthleticVersion2010(2),
	Version2011(3),
	AthleticVersion2011(4),
	Version2011Japan(5);

	private int id;

	private FatMethod(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static FatMethod fromId(int id) {
		for (FatMethod e : values())
			if (e.getId() == id)
				return e;
		return null;
	}
	
};