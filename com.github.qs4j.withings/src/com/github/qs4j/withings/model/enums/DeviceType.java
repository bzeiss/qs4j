package com.github.qs4j.withings.model.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum DeviceType implements Serializable {
	UserRelated(0),
	BodyScale(1),	
	BloodPressureMonitor(4);

	private int id;

	private DeviceType(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static DeviceType fromId(int id) {
		for (DeviceType e : values())
			if (e.getId() == id)
				return e;
		return null;
	}
	
};