package com.github.qs4j.withings.model.enums;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Category implements Serializable {
	Measure(1), 
	Target(2);

	private int id;

	private Category(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static Category fromId(int id) {
		for (Category e : values())
			if (e.getId() == id)
				return e;
		return null;
	}
	
};