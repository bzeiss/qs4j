package com.github.qs4j.withings.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.qs4j.withings.model.serialization.WithingsDateDeserializer;

public class WithingsMeasures implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "updatetime")
	@JsonDeserialize(using = WithingsDateDeserializer.class)
	private Date updateTime;

	@JsonProperty(value = "measuregrps")
	private ArrayList<WithingsMeasureGroup> measureGroups;

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public ArrayList<WithingsMeasureGroup> getMeasureGroups() {
		return measureGroups;
	}

	public void setMeasureGroups(ArrayList<WithingsMeasureGroup> measureGroups) {
		this.measureGroups = measureGroups;
	}

	@Override
	public String toString() {
		return "WithingsMeasures [\nupdateTime=" + updateTime
				+ ",\nmeasureGroups=" + measureGroups + "\n]";
	}

}
