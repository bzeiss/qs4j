package com.github.qs4j.withings.model.serialization;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class WithingsDateDeserializer extends JsonDeserializer<Date> {

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext arg1)
			throws IOException, JsonProcessingException {
		Long value = jp.getLongValue()*1000;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(value);
		return cal.getTime();
	}

}
