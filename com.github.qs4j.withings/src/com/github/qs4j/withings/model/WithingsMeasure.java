package com.github.qs4j.withings.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.qs4j.withings.model.enums.MeasureType;

public class WithingsMeasure implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "value")
	private Long value;

	@JsonProperty(value = "type")
	private MeasureType type;

	@JsonProperty(value = "unit")
	private Long unit;

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public MeasureType getType() {
		return type;
	}

	public void setType(MeasureType type) {
		this.type = type;
	}

	public Long getUnit() {
		return unit;
	}

	public void setUnit(Long unit) {
		this.unit = unit;
	}

	public Double getRealValue() {
		Double result = getValue() * Math.pow(10.0, getUnit());
		return result;
	}
	
	@Override
	public String toString() {
		return "WithingsMeasure [\nvalue=" + value + ",\ntype=" + type + ",\nunit="
				+ unit + ",\nrealValue=" + getRealValue() + "\n]";
	}

}
