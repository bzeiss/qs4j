package com.github.qs4j.withings.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.qs4j.withings.model.enums.FatMethod;
import com.github.qs4j.withings.model.enums.Gender;
import com.github.qs4j.withings.model.serialization.WithingsDateDeserializer;

public class WithingsUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "id")
	private Long id;

	@JsonProperty(value = "firstname")
	private String firstName;

	@JsonProperty(value = "lastname")
	private String lastName;

	@JsonProperty(value = "shortname")
	private String shortName;

	@JsonProperty(value = "gender")
	private Gender gender;

	@JsonProperty(value = "fatmethod")
	private FatMethod fatMethod;

	@JsonProperty(value = "birthdate")
	@JsonDeserialize(using = WithingsDateDeserializer.class)
	private Date birthDate;

	@JsonProperty(value = "ispublic")
	private boolean isPublic;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public FatMethod getFatMethod() {
		return fatMethod;
	}

	public void setFatMethod(FatMethod fatMethod) {
		this.fatMethod = fatMethod;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	@Override
	public String toString() {
		return "WithingsUser [\nid=" + id + ",\nfirstName=" + firstName
				+ ",\nlastName=" + lastName + ",\nshortName=" + shortName
				+ ",\ngender=" + gender + ",\nfatMethod=" + fatMethod
				+ ",\nbirthDate=" + birthDate + ",\nisPublic=" + isPublic + "\n]";
	}

}
