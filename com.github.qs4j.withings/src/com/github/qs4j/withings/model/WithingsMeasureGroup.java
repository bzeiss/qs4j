package com.github.qs4j.withings.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.qs4j.withings.model.enums.AttributionStatus;
import com.github.qs4j.withings.model.enums.Category;
import com.github.qs4j.withings.model.serialization.WithingsDateDeserializer;

public class WithingsMeasureGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "date")
	@JsonDeserialize(using = WithingsDateDeserializer.class)
	private Date dateCreated;

	@JsonProperty(value = "grpid")
	private Long groupId;

	@JsonProperty(value = "attrib")
	private AttributionStatus attributionStatus;

	@JsonProperty(value = "category")
	private Category category;

	@JsonProperty(value = "comment")
	private String comment;

	@JsonProperty(value = "measures")
	private ArrayList<WithingsMeasure> measures;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public AttributionStatus getAttributionStatus() {
		return attributionStatus;
	}

	public void setAttributionStatus(AttributionStatus attributionStatus) {
		this.attributionStatus = attributionStatus;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public ArrayList<WithingsMeasure> getMeasures() {
		return measures;
	}

	public void setMeasures(ArrayList<WithingsMeasure> measures) {
		this.measures = measures;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "WithingsMeasureGroup [\ngroupId=" + groupId
				+ ",\nattributionStatus=" + attributionStatus + ",\ndateCreated="
				+ dateCreated + ",\ncategory=" + category + ",\ncomment="
				+ comment + ",\nmeasures=" + measures + "\n]";
	}

}
