package com.github.qs4j.withings.model.compact;

import java.util.Date;

import com.github.qs4j.withings.model.enums.AttributionStatus;
import com.github.qs4j.withings.model.enums.MeasureType;

public class CompactMeasure {
	private Date date;
	private MeasureType measureType;
	private Double realValue;
	private AttributionStatus attributionStatus;

	public CompactMeasure(Date date, MeasureType measureType, Double realValue,
			AttributionStatus attributionStatus) {
		this.date = date;
		this.measureType = measureType;
		this.realValue = realValue;
		this.attributionStatus = attributionStatus;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getRealValue() {
		return realValue;
	}

	public void setRealValue(Double realValue) {
		this.realValue = realValue;
	}

	public MeasureType getMeasureType() {
		return measureType;
	}

	public void setMeasureType(MeasureType measureType) {
		this.measureType = measureType;
	}

	public AttributionStatus getAttributionStatus() {
		return attributionStatus;
	}

	public void setAttributionStatus(AttributionStatus attributionStatus) {
		this.attributionStatus = attributionStatus;
	}

	@Override
	public String toString() {
		return "CompactMeasure [date=" + date + ", measureType=" + measureType
				+ ", realValue=" + realValue + ", attributionStatus="
				+ attributionStatus + "]";
	}

}
