package com.github.qs4j.withings.model.answers;

import java.io.Serializable;

import com.github.qs4j.withings.WithingsReturnCode;

public abstract class WithingsAnswer implements Serializable {
	private static final long serialVersionUID = 1L;
	private WithingsReturnCode status;

	public WithingsReturnCode getStatus() {
		return status;
	}

	public void setStatus(WithingsReturnCode status) {
		this.status = status;
	}

}
