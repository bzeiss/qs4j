package com.github.qs4j.withings.model.answers;

import com.github.qs4j.withings.model.WithingsMeasures;

public class WithingsMeasuresAnswer extends WithingsAnswer {
	private static final long serialVersionUID = 1L;
	private WithingsMeasures body;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public WithingsMeasures getBody() {
		return body;
	}

	public void setBody(WithingsMeasures body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "WithingsMeasuresAnswer [\nbody=" + body + "\n]";
	}

}
