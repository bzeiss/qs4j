package com.github.qs4j.withings.model.answers;

import java.io.Serializable;
import java.util.ArrayList;

import com.github.qs4j.withings.model.WithingsUser;

public class WithingsUsers implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<WithingsUser> users;

	public ArrayList<WithingsUser> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<WithingsUser> users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "WithingsUserAnswer [\nusers=" + users + "\n]";
	}

}
