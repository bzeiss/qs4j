package com.github.qs4j.withings.model.answers;

public class WithingsUserAnswer extends WithingsAnswer {
	private static final long serialVersionUID = 1L;
	private WithingsUsers body;

	public WithingsUsers getBody() {
		return body;
	}

	public void setBody(WithingsUsers body) {
		this.body = body;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
