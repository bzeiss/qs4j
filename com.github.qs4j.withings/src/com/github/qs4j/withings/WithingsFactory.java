package com.github.qs4j.withings;

public class WithingsFactory {
	private static IWithings instance = null;

	public static IWithings getInstance() {
		if (instance == null)
			instance = new WithingsImpl();
		return instance;
	}

}
