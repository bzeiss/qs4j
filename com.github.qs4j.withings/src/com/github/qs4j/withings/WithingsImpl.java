package com.github.qs4j.withings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import oauth.signpost.signature.HmacSha1MessageSigner;
import oauth.signpost.signature.QueryStringSigningStrategy;

import org.apache.http.client.ClientProtocolException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.qs4j.withings.model.WithingsMeasure;
import com.github.qs4j.withings.model.WithingsMeasureGroup;
import com.github.qs4j.withings.model.WithingsMeasures;
import com.github.qs4j.withings.model.WithingsUser;
import com.github.qs4j.withings.model.answers.WithingsMeasuresAnswer;
import com.github.qs4j.withings.model.answers.WithingsUserAnswer;
import com.github.qs4j.withings.model.compact.CompactMeasure;
import com.github.qs4j.withings.model.enums.Category;
import com.github.qs4j.withings.model.enums.DeviceType;
import com.github.qs4j.withings.model.enums.MeasureType;

public class WithingsImpl implements IWithings {
	public static final String URL_BASE = "http://wbsapi.withings.net/";
	public static final String URL_OAUTH_BASE = "https://oauth.withings.com/";

	private static final String URL_REQUEST_TOKEN = "account/request_token";
	private static final String URL_AUTHORIZE_TOKEN = "account/authorize";
	private static final String URL_ACCESS_TOKEN = "account/access_token";

	private static final String URL_MEASURE = "measure?action=getmeas&userid=";
	private static final String URL_USER_INFORMATION = "user?action=getbyuserid&userid=";

	private static final String PROPS_FILENAME = "qs4j.properties";
	private static final String PROPS_CONSUMER_TOKEN = "withings.consumerToken";
	private static final String PROPS_CONSUMER_TOKEN_SECRET = "withings.consumerTokenSecret";
	private static final String PROPS_USERID = "withings.userId";
	private static final String PROPS_ACCESS_TOKEN = "withings.accessToken";
	private static final String PROPS_ACCESS_TOKEN_SECRET = "withings.accessTokenSecret";
	
	
	private HashMap<Integer, WithingsAccessToken> accessTokensByUserId = new HashMap<Integer, WithingsAccessToken>();

	private String consumerToken;
	private String consumerTokenSecret;

	// -----------------------------------------------------------------------------------------------

	public WithingsImpl() {
	}

	// -----------------------------------------------------------------------------------------------

	public WithingsAccessToken getAccessTokenByCommandLine(String callbackUrl)
			throws WithingsException {
		checkConsumerTokens();
		if  ((callbackUrl == null) || (callbackUrl.length() == 0))
			throw new WithingsException("Callback URL must not be null or empty!", 10000);

		try {
			OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
					consumerToken, consumerTokenSecret);

			OAuthProvider provider = new CommonsHttpOAuthProvider(
					URL_OAUTH_BASE + URL_REQUEST_TOKEN, URL_OAUTH_BASE
							+ URL_ACCESS_TOKEN, URL_OAUTH_BASE
							+ URL_AUTHORIZE_TOKEN);

			String url = provider.retrieveRequestToken(consumer, callbackUrl);
			System.out.println("Go to the following URL and authorize: ");
			System.out.println(url);
			System.out.println();
			System.out.println("Hit return when you are finished!");
			Scanner scanner = new Scanner(System.in);
			scanner.nextLine();
			scanner.close();

			System.out.println("---------------------------------------");
			provider.retrieveAccessToken(consumer, null);

			WithingsAccessToken accessToken = new WithingsAccessToken(
					consumer.getToken(), consumer.getTokenSecret(),
					Integer.parseInt(provider.getResponseParameters()
							.get("userid").toString().replaceAll("\\[", "")
							.replaceAll("\\]", "")));
			System.out.println(accessToken);
			addAccessToken(accessToken);

			return accessToken;
		} catch (NoSuchElementException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthMessageSignerException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthNotAuthorizedException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthExpectationFailedException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthCommunicationException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		}
	}

	// -----------------------------------------------------------------------------------------------

	private void checkConsumerTokens() throws WithingsException {
		if (consumerToken == null)
			throw new WithingsException("Consumer Token has not been set!",
					10000);
		if (consumerTokenSecret == null)
			throw new WithingsException(
					"Consumer Token Secret has not been set!", 10000);
	}

	// -----------------------------------------------------------------------------------------------

	private void checkAccessTokens(WithingsAccessToken accessToken)
			throws WithingsException {
		if (accessToken == null)
			throw new WithingsException(
					"Access Token Object has not been set!", 10000);
		if (accessToken.getAccessToken() == null)
			throw new WithingsException("Access Token has not been set!", 10000);
		if (accessToken.getAccessTokenSecret() == null)
			throw new WithingsException(
					"Access Token Secret has not been set!", 10000);
		if (accessToken.getUserId() == null)
			throw new WithingsException("UserId has not been set!", 10000);
	}

	// -----------------------------------------------------------------------------------------------

	public WithingsUser getUserInformation(Integer userId)
			throws WithingsException {
		WithingsAccessToken currentAccessToken = accessTokensByUserId
				.get(userId);

		checkConsumerTokens();
		checkAccessTokens(currentAccessToken);
		try {
			OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
					consumerToken, consumerTokenSecret);
			consumer.setSigningStrategy(new QueryStringSigningStrategy());
			consumer.setSendEmptyTokens(true);
			consumer.setMessageSigner(new HmacSha1MessageSigner());

			consumer.setTokenWithSecret(currentAccessToken.getAccessToken(),
					currentAccessToken.getAccessTokenSecret());

			String url = URL_BASE + URL_USER_INFORMATION
					+ Integer.toString(currentAccessToken.getUserId());
			String str = WithingsUtil.executeSignedRequest(url, consumer);
			ObjectMapper mapper = new ObjectMapper();

			WithingsUserAnswer answer = mapper.readValue(str,
					WithingsUserAnswer.class);

			if (answer.getStatus() != WithingsReturnCode.OperationSuccessful) {
				throw new WithingsException(
						"The Withings web service has returned an error!",
						answer.getStatus());
			}

			return answer.getBody().getUsers().get(0);
		} catch (OAuthMessageSignerException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthExpectationFailedException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthCommunicationException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (ClientProtocolException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (IOException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		}
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public ArrayList<CompactMeasure> getCompactMeasures(Integer userId,
			Date startDate, Date endDate, DeviceType deviceType,
			MeasureType measureType, Date lastUpdate, Category category,
			Integer limit, Integer offset) throws WithingsException {

		WithingsMeasures measures = getMeasures(userId, startDate, endDate,
				deviceType, measureType, lastUpdate, category, limit, offset);
		ArrayList<CompactMeasure> result = new ArrayList<CompactMeasure>();

		for (int i = 0; i < measures.getMeasureGroups().size(); i++) {
			WithingsMeasureGroup measureGroup = measures.getMeasureGroups()
					.get(i);
			for (int j = 0; j < measureGroup.getMeasures().size(); j++) {
				WithingsMeasure measure = measureGroup.getMeasures().get(j);
				result.add(new CompactMeasure(measureGroup.getDateCreated(),
						measure.getType(), measure.getRealValue(), measureGroup.getAttributionStatus()));
			}
		}

		return result;
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public WithingsMeasures getAllMeasures(Integer userId)
			throws WithingsException {
		return getMeasures(userId, null, null, null, null, null, null, null,
				null);
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public WithingsMeasures getMeasures(Integer userId, Date startDate,
			Date endDate, DeviceType deviceType, MeasureType measureType,
			Date lastUpdate, Category category, Integer limit, Integer offset)
			throws WithingsException {

		WithingsAccessToken currentAccessToken = accessTokensByUserId
				.get(userId);
		checkConsumerTokens();
		checkAccessTokens(currentAccessToken);

		try {
			OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
					consumerToken, consumerTokenSecret);
			consumer.setSigningStrategy(new QueryStringSigningStrategy());
			consumer.setSendEmptyTokens(true);
			consumer.setMessageSigner(new HmacSha1MessageSigner());

			consumer.setTokenWithSecret(currentAccessToken.getAccessToken(),
					currentAccessToken.getAccessTokenSecret());

			String url = URL_BASE + URL_MEASURE
					+ Integer.toString(currentAccessToken.getUserId());
			if (startDate != null) {
				url += "&startdate=" + WithingsUtil.dateToEpoch(startDate);
			}
			if (endDate != null) {
				url += "&enddate=" + WithingsUtil.dateToEpoch(endDate);
			}
			if (deviceType != null) {
				url += "&devtype=" + deviceType.getId();
			}
			if (measureType != null) {
				url += "&meastype=" + measureType.getId();
			}
			if (lastUpdate != null) {
				url += "&lastupdate=" + WithingsUtil.dateToEpoch(lastUpdate);
			}
			if (category != null) {
				url += "&category=" + category.getId();
			}
			if (limit != null) {
				url += "&limit=" + limit;
			}
			if (offset != null) {
				url += "&offset=" + offset;
			}

			String str = WithingsUtil.executeSignedRequest(url, consumer);

			ObjectMapper mapper = new ObjectMapper();

			WithingsMeasuresAnswer answer = mapper.readValue(str,
					WithingsMeasuresAnswer.class);

			if (answer.getStatus() != WithingsReturnCode.OperationSuccessful) {
				throw new WithingsException(
						"The Withings web service has returned an error!",
						answer.getStatus());
			}

			return answer.getBody();
		} catch (OAuthMessageSignerException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthExpectationFailedException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (OAuthCommunicationException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (ClientProtocolException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (IOException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		}

	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public void addAccessToken(String accessToken, String accessTokenSecret,
			int userId) {
		this.accessTokensByUserId.put(userId, new WithingsAccessToken(
				accessToken, accessTokenSecret, userId));
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public void addAccessToken(WithingsAccessToken accessToken) {
		this.accessTokensByUserId.put(accessToken.getUserId(), accessToken);
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public void setConsumerToken(String consumerToken,
			String consumerTokenSecret) {
		this.consumerToken = consumerToken;
		this.consumerTokenSecret = consumerTokenSecret;
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public void loadConfigFile(String pathPrefix) throws WithingsException {
		try {
			Properties props = new Properties();
			FileInputStream fis;
			if (pathPrefix == null)
				fis = new FileInputStream(
					PROPS_FILENAME);
			else
				fis = new FileInputStream(
						pathPrefix + PROPS_FILENAME);
			
			props.load(fis);

			String consumerToken = (String) props.get(PROPS_CONSUMER_TOKEN);
			if (consumerToken != null) {
				this.consumerToken = consumerToken;
				props.remove(PROPS_CONSUMER_TOKEN);
			}
			String consumerTokenSecret = (String) props
					.get(PROPS_CONSUMER_TOKEN_SECRET);
			if (consumerToken != null) {
				this.consumerTokenSecret = consumerTokenSecret;
				props.remove(PROPS_CONSUMER_TOKEN_SECRET);
			}

			if ((props.get(PROPS_USERID) != null)
					&& (props.get(PROPS_ACCESS_TOKEN) != null)
					&& (props.get(PROPS_ACCESS_TOKEN_SECRET) != null)) {
				String[] userIds = ((String) props.get(PROPS_USERID)).split(",");
				String[] accessTokens = ((String) props.get(PROPS_ACCESS_TOKEN)).split(",");
				String[] accessTokenSecrets = ((String) props.get(PROPS_ACCESS_TOKEN_SECRET)).split(",");
				if (userIds.length != accessTokens.length || userIds.length != accessTokenSecrets.length) {
					throw new WithingsException("Problem in " + PROPS_FILENAME + ": list count of access tokens mismatch!", 10000);
				}
				for (int i=0; i < userIds.length; i++) {
					WithingsAccessToken accessToken = new WithingsAccessToken(accessTokens[i], accessTokenSecrets[i], Integer.parseInt((String)userIds[i]));
					addAccessToken(accessToken);
				}
			}
			
			fis.close();
		} catch (IOException e) {
			throw new WithingsException(pathPrefix + " " + PROPS_FILENAME + " does not exist!", 10000);
		}
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public boolean existsConfigFile(String pathPrefix) throws WithingsException {
		File f;
		if (pathPrefix == null)
			f = new File(PROPS_FILENAME);
		else
			f = new File(pathPrefix + PROPS_FILENAME);
		return f.exists();
	}

	// -----------------------------------------------------------------------------------------------

	@Override
	public void saveConfigFile(String pathPrefix) throws WithingsException {
		Properties props = new Properties();
		props.put(PROPS_CONSUMER_TOKEN, consumerToken);
		props.put(PROPS_CONSUMER_TOKEN_SECRET, consumerTokenSecret);
		StringBuffer userIds = new StringBuffer();
		StringBuffer accessTokens = new StringBuffer();
		StringBuffer accessTokenSecrets = new StringBuffer();
		Iterator<WithingsAccessToken> it = accessTokensByUserId.values().iterator();
		int counter = 0;
		while (it.hasNext()) {
			WithingsAccessToken value = it.next();
			if (counter > 0) {
				userIds.append(",");
				accessTokens.append(",");
				accessTokenSecrets.append(",");
			}
			userIds.append(value.getUserId());
			accessTokens.append(value.getAccessToken());
			accessTokenSecrets.append(value.getAccessTokenSecret());
			counter++;
		}
		props.put(PROPS_USERID, userIds.toString());
		props.put(PROPS_ACCESS_TOKEN, accessTokens.toString());
		props.put(PROPS_ACCESS_TOKEN_SECRET, accessTokenSecrets.toString());
		
		File f;
		if (pathPrefix == null)
			f = new File(PROPS_FILENAME);
		else
			f = new File(pathPrefix + PROPS_FILENAME);

		try {
			OutputStream out = new FileOutputStream(f);
	        props.store(out, "qs4j properties file");
		} catch (FileNotFoundException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		} catch (IOException e) {
			throw new WithingsException(e.getLocalizedMessage(), 10000);
		}
	}

}
