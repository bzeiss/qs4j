package com.github.qs4j.withings.examples;

import com.github.qs4j.withings.IWithings;
import com.github.qs4j.withings.WithingsException;
import com.github.qs4j.withings.WithingsFactory;
import com.github.qs4j.withings.model.WithingsUser;

public class UserInformation {
	public static void main(String[] args) {
		IWithings withings = WithingsFactory.getInstance();
		int userId = 12345;
				
		try {
			withings.loadConfigFile("");
			WithingsUser user = withings.getUserInformation(userId);
			System.out.println(user.toString());
			
		} catch (WithingsException e) {
			e.printStackTrace();
		}
		
	}

}
