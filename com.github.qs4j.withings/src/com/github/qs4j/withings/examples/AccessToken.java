package com.github.qs4j.withings.examples;

import com.github.qs4j.withings.IWithings;
import com.github.qs4j.withings.WithingsException;
import com.github.qs4j.withings.WithingsFactory;

public class AccessToken {
	public static void main(String[] args) {
		IWithings withings = WithingsFactory.getInstance();
				
		try {
			if (withings.existsConfigFile(""))
				withings.loadConfigFile("");
			
			withings.getAccessTokenByCommandLine("http://www.yourcallbackurl.com");
			
			withings.saveConfigFile("");
			System.out.println("Access token stored to config file.");
		} catch (WithingsException e) {
			e.printStackTrace();
		}
	}

}
