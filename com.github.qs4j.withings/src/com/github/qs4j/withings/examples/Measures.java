package com.github.qs4j.withings.examples;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.github.qs4j.withings.IWithings;
import com.github.qs4j.withings.WithingsException;
import com.github.qs4j.withings.WithingsFactory;
import com.github.qs4j.withings.model.compact.CompactMeasure;
import com.github.qs4j.withings.model.enums.MeasureType;

public class Measures {
	public static void main(String[] args) {
		IWithings withings = WithingsFactory.getInstance();
		int userId = 12345;
				
		try {
			if (withings.existsConfigFile(""))
				withings.loadConfigFile("");
			
//			WithingsMeasures allMeasures = withings.getAllMeasures();
//			System.out.println(allMeasures);
			
			Date someTimeAgo = new Date();
			someTimeAgo.setTime(Calendar.getInstance().getTimeInMillis() - 6*1000*60*60*24);
			System.out.println("Searching since " + someTimeAgo.toString());
			
//			WithingsMeasures filteredMeasures = withings.getMeasures(yesterday, null, null, MeasureType.WeightKg, null, null, null, null);
//			System.out.println(filteredMeasures);
			
			ArrayList<CompactMeasure> weightMeasures = withings.getCompactMeasures(userId, someTimeAgo, null, null, MeasureType.WeightKg, null, null, null, null);
			for (int i=0; i < weightMeasures.size(); i++) {
				System.out.println(weightMeasures.get(i));
			}
			
		} catch (WithingsException e) {
			e.printStackTrace();
		}
		
	}

}
