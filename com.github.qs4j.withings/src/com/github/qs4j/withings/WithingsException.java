package com.github.qs4j.withings;

import java.io.IOException;

public class WithingsException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String message;
	private WithingsReturnCode code;

	public WithingsException(IOException ex) {
		this.initCause(ex);
	}

	public WithingsException(String message, int code) {
		this.message = message;
		this.code = WithingsReturnCode.fromId(code);
	}

	public WithingsException(String message, WithingsReturnCode code) {
		this.message = message;
		this.code = code;
	}

	@Override
	public String getMessage() {
		return this.message + " [" + this.code.getId() + " - " + this.code.toString() + "]";
	}

	public WithingsReturnCode getCode() {
		return this.code;
	}
	
}
