package com.github.qs4j.withings;

public class WithingsAccessToken {

	private String accessToken;
	private String accessTokenSecret;
	private Integer userId;

	public WithingsAccessToken(String accessToken, String accessTokenSecret, int userId) {
		this.accessToken = accessToken;
		this.accessTokenSecret = accessTokenSecret;
		this.userId = userId;
	}
	
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccessTokenSecret() {
		return accessTokenSecret;
	}

	public void setAccessTokenSecret(String accessTokenSecret) {
		this.accessTokenSecret = accessTokenSecret;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "WithingsAccessToken [\naccessToken=" + accessToken
				+ "\naccessTokenSecret=" + accessTokenSecret + "\nuserId="
				+ userId + "\n]";
	}

}
