package com.github.qs4j.withings;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum WithingsReturnCode {
	OperationSuccessful(0),
	UnknownErrorOccured(2555),
	UserIdProvidedIsAbsentOrIncorrect(247),
	ProvidedUserIdAndOAuthCredentialsMismatch(250),
	NoSuchSubscriptionFound(286),
	CallbackUrlAbsentOrIncorrect(293),
	NoSuchSubscriptionCouldBeDeletected(294),
	CommentAbsentOrIncorrect(304),
	TooManyNotificationsAlreadySent(305),
	OAuthSignatureInvalid(342),
	NoNotificationMatchingTheCriteraFound(343),
	QS4JError(10000);
	
	private int id;

	private WithingsReturnCode(int id) {
		this.id = id;
	}

	@JsonValue
	public int getId() {
		return this.id;
	}

	@JsonCreator
	public static WithingsReturnCode fromId(int id) {
		for (WithingsReturnCode e : values())
			if (e.getId() == id)
				return e;
		return null;
	}
}
