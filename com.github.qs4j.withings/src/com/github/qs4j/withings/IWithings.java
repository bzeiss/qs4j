package com.github.qs4j.withings;

import java.util.ArrayList;
import java.util.Date;

import com.github.qs4j.withings.model.WithingsMeasures;
import com.github.qs4j.withings.model.WithingsUser;
import com.github.qs4j.withings.model.compact.CompactMeasure;
import com.github.qs4j.withings.model.enums.Category;
import com.github.qs4j.withings.model.enums.DeviceType;
import com.github.qs4j.withings.model.enums.MeasureType;

public interface IWithings {
	public WithingsAccessToken getAccessTokenByCommandLine(String callbackUrl) throws WithingsException;

	public WithingsUser getUserInformation(Integer userId) throws WithingsException;
	public WithingsMeasures getAllMeasures(Integer userId) throws WithingsException;
	public WithingsMeasures getMeasures(Integer userId, Date startDate, Date endDate, DeviceType deviceType, MeasureType measureType, Date lastUpdate, Category category, Integer limit, Integer offset) throws WithingsException;
	public ArrayList<CompactMeasure> getCompactMeasures(Integer userId, Date startDate, Date endDate, DeviceType deviceType, MeasureType measureType, Date lastUpdate, Category category, Integer limit, Integer offset) throws WithingsException;

	public boolean existsConfigFile(String pathPrefix) throws WithingsException;
	public void saveConfigFile(String pathPrefix) throws WithingsException;
	public void loadConfigFile(String pathPrefix) throws WithingsException;
	public void setConsumerToken(String consumerToken, String consumerTokenSecret);	
	public void addAccessToken(String accessToken, String accessTokenSecret, int userId);
	public void addAccessToken(WithingsAccessToken accessToken);
	
}
