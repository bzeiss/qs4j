package com.github.qs4j.withings.datasource.ui.impl;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import com.github.qs4j.withings.datasource.impl.WithingsQueryFilter;
import com.github.qs4j.withings.model.enums.Category;
import com.github.qs4j.withings.model.enums.MeasureType;
import com.github.qs4j.withings.model.enums.DeviceType;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;

public class QueryPageControl extends Composite {
	private DataBindingContext m_bindingContext;
	private Button btnStartDateFilterEnabled;
	private Button btnEndDateFilterEnabled;
	private Button btnDeviceTypeFilterEnabled;
	private Button btnLastUpdateFilterEnabled;
	private Button btnCategoryFilterEnabled;
	private Button btnLimitFilterEnabled;
	private Button btnOffsetFilterEnabled;
	private Button btnMeasureTypeFilterEnabled;
	private DateTime startDate;
	private DateTime endDate;
	private DateTime lastUpdateDate;
	private DateTime lastUpdateTime;
	private DateTime endTime;
	private DateTime startTime;
	private ComboViewer deviceTypeComboViewer;
	private ComboViewer categoryComboViewer;
	private ComboViewer measureTypeComboViewer;
	private Spinner limitSpinner;
	private Spinner offsetSpinner;
	private WithingsQueryFilter withingsQueryFilter = new WithingsQueryFilter();

	public QueryPageControl(Composite parent, int style, WithingsQueryFilter withingsQueryFilter) {
		super(parent, style);

		if (withingsQueryFilter != null)
			this.withingsQueryFilter = withingsQueryFilter;

		setLayout(new GridLayout(4, false));

		Label lblStartDate = new Label(this, SWT.NONE);
		lblStartDate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblStartDate.setText("Start date:");

		btnStartDateFilterEnabled = new Button(this, SWT.CHECK);
		btnStartDateFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleStartDateEnablement();
			}
		});
		btnStartDateFilterEnabled.setText("Enabled");

		startDate = new DateTime(this, SWT.BORDER | SWT.DROP_DOWN);
		startDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		startTime = new DateTime(this, SWT.BORDER | SWT.TIME);
		startTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
				1, 1));

		Label lblEndDate = new Label(this, SWT.NONE);
		lblEndDate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblEndDate.setText("End date:");

		btnEndDateFilterEnabled = new Button(this, SWT.CHECK);
		btnEndDateFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleEndDateEnablement();
			}
		});
		btnEndDateFilterEnabled.setText("Enabled");

		endDate = new DateTime(this, SWT.BORDER | SWT.DROP_DOWN);
		endDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1));

		endTime = new DateTime(this, SWT.BORDER | SWT.TIME);
		endTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false,
				1, 1));

		Label lblDeviceType = new Label(this, SWT.NONE);
		lblDeviceType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblDeviceType.setText("Device type:");

		btnDeviceTypeFilterEnabled = new Button(this, SWT.CHECK);
		btnDeviceTypeFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleDeviceTypeEnablement();
			}
		});
		btnDeviceTypeFilterEnabled.setText("Enabled");

		deviceTypeComboViewer = new ComboViewer(this, SWT.READ_ONLY);
		Combo deviceTypeCombo = deviceTypeComboViewer.getCombo();
		deviceTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		new Label(this, SWT.NONE);

		deviceTypeComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		deviceTypeComboViewer.setInput(DeviceType.values());
		
		Label lblMeasureType = new Label(this, SWT.NONE);
		lblMeasureType.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblMeasureType.setText("Measure type:");
		
		btnMeasureTypeFilterEnabled = new Button(this, SWT.CHECK);
		btnMeasureTypeFilterEnabled.setText("Enabled");

		btnMeasureTypeFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleMeasureTypeEnablement();
			}
		});
		
		measureTypeComboViewer = new ComboViewer(this, SWT.READ_ONLY);
		Combo measureTypeCombo = measureTypeComboViewer.getCombo();
		measureTypeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		measureTypeComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		measureTypeComboViewer.setInput(MeasureType.values());
		
		new Label(this, SWT.NONE);

		Label lblLastUpdate = new Label(this, SWT.NONE);
		lblLastUpdate.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblLastUpdate.setText("Entries updated later than:");

		btnLastUpdateFilterEnabled = new Button(this, SWT.CHECK);
		btnLastUpdateFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleLastUpdateEnablement();
			}
		});
		btnLastUpdateFilterEnabled.setText("Enabled");

		lastUpdateDate = new DateTime(this, SWT.BORDER | SWT.DROP_DOWN);
		lastUpdateDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));

		lastUpdateTime = new DateTime(this, SWT.BORDER | SWT.TIME);
		lastUpdateTime.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));

		Label lblCategory = new Label(this, SWT.NONE);
		lblCategory.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblCategory.setText("Category:");

		btnCategoryFilterEnabled = new Button(this, SWT.CHECK);
		btnCategoryFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleCategoryEnablement();
			}
		});
		btnCategoryFilterEnabled.setText("Enabled");

		categoryComboViewer = new ComboViewer(this, SWT.READ_ONLY);
		Combo categoryCombo = categoryComboViewer.getCombo();
		categoryCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
				false, 1, 1));

		categoryComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		categoryComboViewer.setInput(Category.values());
		
		new Label(this, SWT.NONE);

		Label lblLimit = new Label(this, SWT.NONE);
		lblLimit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblLimit.setText("Limit:");

		btnLimitFilterEnabled = new Button(this, SWT.CHECK);
		btnLimitFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleLimitEnablement();
			}
		});
		btnLimitFilterEnabled.setText("Enabled");

		limitSpinner = new Spinner(this, SWT.BORDER);
		limitSpinner.setMaximum(999999999);
		limitSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		new Label(this, SWT.NONE);

		Label lblOffset = new Label(this, SWT.NONE);
		lblOffset.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblOffset.setText("Offset:");

		btnOffsetFilterEnabled = new Button(this, SWT.CHECK);
		btnOffsetFilterEnabled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				toggleOffsetEnablement();
			}
		});
		btnOffsetFilterEnabled.setText("Enabled");

		offsetSpinner = new Spinner(this, SWT.BORDER);
		offsetSpinner.setMaximum(999999999);
		offsetSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false,
				false, 1, 1));
		new Label(this, SWT.NONE);

		m_bindingContext = initDataBindings();

		toggleStartDateEnablement();
		toggleEndDateEnablement();
		toggleLastUpdateEnablement();

		toggleDeviceTypeEnablement();
		toggleCategoryEnablement();

		toggleOffsetEnablement();
		toggleLimitEnablement();
		toggleMeasureTypeEnablement();
	}

	@Override
	protected void checkSubclass() {
	}


	public WithingsQueryFilter getWithingsQueryFilter() {
		return withingsQueryFilter;
	}

	public void setWithingsQueryFilter(WithingsQueryFilter withingsQueryFilter) {
		this.withingsQueryFilter = withingsQueryFilter;
	}

	private void toggleStartDateEnablement() {
		if (btnStartDateFilterEnabled.getSelection()) {
			startDate.setEnabled(true);
			startTime.setEnabled(true);
		} else {
			startDate.setEnabled(false);
			startTime.setEnabled(false);
		}
		update();
	}

	private void toggleEndDateEnablement() {
		if (btnEndDateFilterEnabled.getSelection()) {
			endDate.setEnabled(true);
			endTime.setEnabled(true);
		} else {
			endDate.setEnabled(false);
			endTime.setEnabled(false);
		}
		update();
	}

	private void toggleLastUpdateEnablement() {
		if (btnLastUpdateFilterEnabled.getSelection()) {
			lastUpdateDate.setEnabled(true);
			lastUpdateTime.setEnabled(true);
		} else {
			lastUpdateDate.setEnabled(false);
			lastUpdateTime.setEnabled(false);
		}
		update();
	}

	private void toggleDeviceTypeEnablement() {
		if (btnDeviceTypeFilterEnabled.getSelection()) {
			deviceTypeComboViewer.getCombo().setEnabled(true);
		} else {
			deviceTypeComboViewer.getCombo().setEnabled(false);
		}
		update();
	}

	private void toggleCategoryEnablement() {
		if (btnCategoryFilterEnabled.getSelection()) {
			categoryComboViewer.getCombo().setEnabled(true);
		} else {
			categoryComboViewer.getCombo().setEnabled(false);
		}
		update();
	}

	private void toggleMeasureTypeEnablement() {
		if (btnMeasureTypeFilterEnabled.getSelection()) {
			measureTypeComboViewer.getCombo().setEnabled(true);
		} else {
			measureTypeComboViewer.getCombo().setEnabled(false);
		}
		update();
	}

	private void toggleLimitEnablement() {
		if (btnLimitFilterEnabled.getSelection()) {
			limitSpinner.setEnabled(true);
		} else {
			limitSpinner.setEnabled(false);
		}
		update();
	}

	private void toggleOffsetEnablement() {
		if (btnOffsetFilterEnabled.getSelection()) {
			offsetSpinner.setEnabled(true);
		} else {
			offsetSpinner.setEnabled(false);
		}
		update();
	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		IObservableValue observeSelectionOffsetSpinnerObserveWidget = WidgetProperties.selection().observe(offsetSpinner);
		IObservableValue offsetWithingsQueryFilterObserveValue = PojoProperties.value("offset").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionOffsetSpinnerObserveWidget, offsetWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionLimitSpinnerObserveWidget = WidgetProperties.selection().observe(limitSpinner);
		IObservableValue limitWithingsQueryFilterObserveValue = PojoProperties.value("limit").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionLimitSpinnerObserveWidget, limitWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSingleSelectionMeasureTypeComboViewer = ViewerProperties.singleSelection().observe(measureTypeComboViewer);
		IObservableValue measureTypeWithingsQueryFilterObserveValue = PojoProperties.value("measureType").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSingleSelectionMeasureTypeComboViewer, measureTypeWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSingleSelectionDeviceTypeComboViewer = ViewerProperties.singleSelection().observe(deviceTypeComboViewer);
		IObservableValue deviceTypeWithingsQueryFilterObserveValue = PojoProperties.value("deviceType").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSingleSelectionDeviceTypeComboViewer, deviceTypeWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSingleSelectionCategoryComboViewer = ViewerProperties.singleSelection().observe(categoryComboViewer);
		IObservableValue categoryWithingsQueryFilterObserveValue = PojoProperties.value("category").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSingleSelectionCategoryComboViewer, categoryWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnStartDateFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnStartDateFilterEnabled);
		IObservableValue startDateFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("startDateEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnStartDateFilterEnabledObserveWidget, startDateFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnEndDateFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnEndDateFilterEnabled);
		IObservableValue endDateFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("endDateEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnEndDateFilterEnabledObserveWidget, endDateFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnDeviceTypeFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnDeviceTypeFilterEnabled);
		IObservableValue deviceTypeFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("deviceTypeEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnDeviceTypeFilterEnabledObserveWidget, deviceTypeFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnLastUpdateFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnLastUpdateFilterEnabled);
		IObservableValue updateAfterDateFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("updateAfterDateEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnLastUpdateFilterEnabledObserveWidget, updateAfterDateFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnCategoryFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnCategoryFilterEnabled);
		IObservableValue categoryFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("categoryEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnCategoryFilterEnabledObserveWidget, categoryFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnLimitFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnLimitFilterEnabled);
		IObservableValue limitFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("limitEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnLimitFilterEnabledObserveWidget, limitFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnOffsetFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnOffsetFilterEnabled);
		IObservableValue offsetFilterEnabledWithingsQueryFilterObserveValue = PojoProperties.value("offsetEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnOffsetFilterEnabledObserveWidget, offsetFilterEnabledWithingsQueryFilterObserveValue, null, null);
		//
		IObservableValue observeSelectionBtnMeasureTypeFilterEnabledObserveWidget = WidgetProperties.selection().observe(btnMeasureTypeFilterEnabled);
		IObservableValue measureTypeEnabledWithingsQueryFilterObserveValue = PojoProperties.value("measureTypeEnabled").observe(withingsQueryFilter);
		bindingContext.bindValue(observeSelectionBtnMeasureTypeFilterEnabledObserveWidget, measureTypeEnabledWithingsQueryFilterObserveValue, null, null);
		//
		return bindingContext;
	}
}
