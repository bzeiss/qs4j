package com.github.qs4j.withings.datasource.ui.impl;

import org.eclipse.datatools.connectivity.oda.design.DataSetDesign;
import org.eclipse.datatools.connectivity.oda.design.ui.wizards.DataSetWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.github.qs4j.withings.datasource.impl.WithingsQueryFilter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class CustomDataSetWizardPage extends DataSetWizardPage {
	private static String DEFAULT_MESSAGE = "Define the Withings query";
	private QueryPageControl queryPageComposite;

	// -----------------------------------------------------------------------------

	public CustomDataSetWizardPage(String pageName) {
		super(pageName);
		setTitle(pageName);
		setMessage(DEFAULT_MESSAGE);
	}

	// -----------------------------------------------------------------------------

	@Override
	public void createPageCustomControl(Composite parent) {
		queryPageComposite = new QueryPageControl(parent, SWT.NONE, parseQueryText());
		
		setControl(queryPageComposite);
		setPageComplete(true);
	}

	// -----------------------------------------------------------------------------

	protected DataSetDesign collectDataSetDesign(DataSetDesign design) {
		XStream xstream = new XStream(new StaxDriver());
		String xml = "";
		if (queryPageComposite != null && queryPageComposite.getWithingsQueryFilter() != null) {
			xml = xstream.toXML(queryPageComposite.getWithingsQueryFilter());
		} else {
			xml = xstream.toXML(new WithingsQueryFilter());
		}
		design.setQueryText(xml);

		return design;
	}

	// -----------------------------------------------------------------------------

	private WithingsQueryFilter parseQueryText() {
		DataSetDesign dataSetDesign = getInitializationDesign();
		if (dataSetDesign == null)
			return new WithingsQueryFilter();

		String queryText = dataSetDesign.getQueryText();
		if ((queryText == null) || (queryText.length() < 1))
			return new WithingsQueryFilter();
		
		XStream xstream = new XStream(new StaxDriver());
		WithingsQueryFilter withingsQueryFilter = (WithingsQueryFilter) xstream.fromXML(queryText);
		
		return withingsQueryFilter;
	}

	//
	// private static String DEFAULT_MESSAGE =
	// "Define the withings web service call filters";
	//
	// private Composite queryPageComposite;
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// public CustomDataSetWizardPage( String pageName )
	// {
	// super( pageName );
	// setTitle( pageName );
	// setMessage( DEFAULT_MESSAGE );
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// public CustomDataSetWizardPage( String pageName, String title,
	// ImageDescriptor titleImage )
	// {
	// super( pageName, title, titleImage );
	// setMessage( DEFAULT_MESSAGE );
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// public void createPageCustomControl( Composite parent )
	// {
	// setControl( createPageControl( parent ) );
	// initializeControl();
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// private Control createPageControl( Composite parent )
	// {
	// queryPageComposite = new QueryPageControl(parent, SWT.NONE);
	//
	// setPageComplete( false );
	//
	// return queryPageComposite;
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// private void initializeControl( )
	// {
	// /*
	// * To optionally restore the designer state of the previous design
	// session, use
	// * getInitializationDesignerState();
	// */
	//
	// // Restores the last saved data set design
	// DataSetDesign dataSetDesign = getInitializationDesign();
	// if( dataSetDesign == null )
	// return; // nothing to initialize
	//
	// // String queryText = dataSetDesign.getQueryText();
	// // if( queryText == null )
	// // return; // nothing to initialize
	//
	// // initialize control
	// //TODO: commmented out
	// //m_queryTextField.setText( queryText );
	// //validateData();
	// setMessage( DEFAULT_MESSAGE );
	//
	// /*
	// * To optionally honor the request for an editable or
	// * read-only design session, use
	// * isSessionEditable();
	// */
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// // private String getQueryText( )
	// // {
	// // return m_queryTextField.getText();
	// // }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// protected DataSetDesign collectDataSetDesign( DataSetDesign design )
	// {
	// if (getControl() == null)
	// return design;
	//
	// if (!hasValidData())
	// return null;
	//
	// savePage(design);
	//
	// return design;
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// protected void collectResponseState( )
	// {
	// super.collectResponseState( );
	// /*
	// * To optionally assign a custom response state, for inclusion in the ODA
	// * design session response, use
	// * setResponseSessionStatus( SessionStatus status );
	// * setResponseDesignerState( DesignerState customState );
	// */
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// protected boolean canLeave( )
	// {
	// return isPageComplete();
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// private void validateData( )
	// {
	// // boolean isValid = ( m_queryTextField != null &&
	// // getQueryText() != null && getQueryText().trim().length() > 0 );
	//
	// boolean isValid = true;
	//
	// if( isValid )
	// setMessage( DEFAULT_MESSAGE );
	// else
	// setMessage( "Requires input value.", ERROR );
	//
	// setPageComplete( isValid );
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// private boolean hasValidData( )
	// {
	// validateData( );
	//
	// return canLeave();
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// private void savePage( DataSetDesign dataSetDesign )
	// {
	// try {
	// ResultSetMetaData resultSetMetaData = new ResultSetMetaData();
	// IParameterMetaData parameterMetaData = new ParameterMetaData();
	//
	// updateResultSetDesign( resultSetMetaData, dataSetDesign );
	// updateParameterDesign( parameterMetaData, dataSetDesign );
	// } catch (OdaException e) {
	// e.printStackTrace();
	// }
	//
	// // // TODO:save user-defined query text
	// //// String queryText = getQueryText();
	// ////
	// //// dataSetDesign.setQueryText( queryText );
	// //
	// // // obtain query's current runtime metadata, and maps it to the
	// dataSetDesign
	// // IConnection customConn = null;
	// // try
	// // {
	// // // instantiate your custom ODA runtime driver class
	// // /* Note: You may need to manually update your ODA runtime extension's
	// // * plug-in manifest to export its package for visibility here.
	// // */
	// // IDriver customDriver = new
	// com.github.qs4j.withings.datasource.impl.Driver();
	// //
	// // // obtain and open a live connection
	// // customConn = customDriver.getConnection( null );
	// // java.util.Properties connProps =
	// // DesignSessionUtil.getEffectiveDataSourceProperties(
	// // getInitializationDesign().getDataSourceDesign() );
	// // customConn.open( connProps );
	// //
	// // // update the data set design with the
	// // // query's current runtime metadata
	// // //TODO:updateDesign( dataSetDesign, customConn, queryText );
	// // }
	// // catch( OdaException e )
	// // {
	// // // not able to get current metadata, reset previous derived metadata
	// // dataSetDesign.setResultSets( null );
	// // dataSetDesign.setParameters( null );
	// //
	// // e.printStackTrace();
	// // }
	// // finally
	// // {
	// // closeConnection( customConn );
	// // }
	// }
	//
	// // //
	// -----------------------------------------------------------------------------
	// //
	// // private void updateDesign( DataSetDesign dataSetDesign,
	// // IConnection conn, String queryText )
	// // throws OdaException
	// // {
	// // IQuery query = conn.newQuery( null );
	// // query.prepare( queryText );
	// //
	// // // TODO a runtime driver might require a query to first execute before
	// // // its metadata is available
	// //// query.setMaxRows( 1 );
	// //// query.executeQuery();
	// //
	// // try
	// // {
	// // IResultSetMetaData md = query.getMetaData();
	// // updateResultSetDesign( md, dataSetDesign );
	// // }
	// // catch( OdaException e )
	// // {
	// // // no result set definition available, reset previous derived metadata
	// // dataSetDesign.setResultSets( null );
	// // e.printStackTrace();
	// // }
	// //
	// // // proceed to get parameter design definition
	// // try
	// // {
	// // IParameterMetaData paramMd = query.getParameterMetaData();
	// // updateParameterDesign( paramMd, dataSetDesign );
	// // }
	// // catch( OdaException ex )
	// // {
	// // // no parameter definition available, reset previous derived metadata
	// // dataSetDesign.setParameters( null );
	// // ex.printStackTrace();
	// // }
	// //
	// // /*
	// // * See DesignSessionUtil for more convenience methods
	// // * to define a data set design instance.
	// // */
	// // }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// /**
	// * Updates the specified data set design's result set definition based on
	// the
	// * specified runtime metadata.
	// * @param md runtime result set metadata instance
	// * @param dataSetDesign data set design instance to update
	// * @throws OdaException
	// */
	// private void updateResultSetDesign( IResultSetMetaData md,
	// DataSetDesign dataSetDesign )
	// throws OdaException
	// {
	// ResultSetColumns columns = DesignSessionUtil.toResultSetColumnsDesign( md
	// );
	//
	// ResultSetDefinition resultSetDefn = DesignFactory.eINSTANCE
	// .createResultSetDefinition();
	// // resultSetDefn.setName( value ); // result set name
	// resultSetDefn.setResultSetColumns( columns );
	//
	// // no exception in conversion; go ahead and assign to specified
	// dataSetDesign
	// dataSetDesign.setPrimaryResultSet( resultSetDefn );
	// dataSetDesign.getResultSets().setDerivedMetaData( true );
	// }
	//
	// //
	// -----------------------------------------------------------------------------
	//
	// /**
	// * Updates the specified data set design's parameter definition based on
	// the
	// * specified runtime metadata.
	// * @param paramMd runtime parameter metadata instance
	// * @param dataSetDesign data set design instance to update
	// * @throws OdaException
	// */
	// private void updateParameterDesign( IParameterMetaData paramMd,
	// DataSetDesign dataSetDesign )
	// throws OdaException
	// {
	// DataSetParameters paramDesign =
	// DesignSessionUtil.toDataSetParametersDesign( paramMd,
	// DesignSessionUtil.toParameterModeDesign(
	// IParameterMetaData.parameterModeIn ) );
	//
	// // no exception in conversion; go ahead and assign to specified
	// dataSetDesign
	// dataSetDesign.setParameters( paramDesign );
	// if( paramDesign == null )
	// return; // no parameter definitions; done with update
	//
	// paramDesign.setDerivedMetaData( true );
	//
	// // TODO replace below with data source specific implementation;
	// // hard-coded parameter's default value for demo purpose
	// if( paramDesign.getParameterDefinitions().size() > 0 )
	// {
	// ParameterDefinition paramDef =
	// (ParameterDefinition) paramDesign.getParameterDefinitions().get( 0 );
	// if( paramDef != null )
	// paramDef.setDefaultScalarValue( "dummy default value" );
	// }
	// }
	//
	// // //
	// -----------------------------------------------------------------------------
	// //
	// // private void closeConnection( IConnection conn )
	// // {
	// // try {
	// // if( conn != null && conn.isOpen() )
	// // conn.close();
	// // }
	// // catch ( OdaException e ) {
	// // e.printStackTrace();
	// // }
	// // }
	//
	// //
	// -----------------------------------------------------------------------------
	//
}
