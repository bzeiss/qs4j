qs4J
=================

qs4J is a Java based project that aims at giving you full control over the data collected by your health gadgets. 
It provides the necessary tools to access and evaluate the data.

### Currently Implemented

 - [Withings API](http://www.withings.com/api) Java library. No support for notifications.
 - Withings data source for use with Eclipse BIRT (fully functional except for input parameters).

I will make first binaries available as soon as I have the Withings stuff feature complete and tested for a while.

### Implementation Plans (in order)

 - [Runkeeper Health Graph API](http://developer.runkeeper.com/healthgraph) Java library.
 - Eclipse BIRT Data Source for the Runkeeper API.
 - [Fitbit API](http://dev.fitbit.com) Java library adapter (there exists a LGPL Java library already).
 - Eclipse BIRT Data Source for the FitBit API.

### Goals

 - Broad support for a wide range of health services so that consolidation of data is not that hard any more.
 - Preparation of data sources for reporting / business intelligence tools to allow easy cross service statistics of the health data (e.g. with [Eclipse BIRT](www.eclipse.org/birt)).

### License

qs4j is licensed under the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt).

### Team

qs4j is currently developed by a single person in his spare time. Please let me know if you are interested in joining the development. Some help would be greatly appreciated. Or, if you just like the project, don't hesistate to send me a greeting mail.

### Dependencies

qs4J makes use of the following dependencies:

 - [Commons Codec](http://commons.apache.org/codec/) ([Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt))
 - [Commons Logging](http://commons.apache.org/logging/) ([Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt))
 - [Commons HTTP Client 4](http://hc.apache.org/httpclient-3.x/) ([Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt))
 - [Jackson](http://wiki.fasterxml.com/JacksonHome) ([Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)/[LGPL 2.1](http://www.gnu.org/licenses/lgpl-3.0.txt))
 - [Signpost](http://code.google.com/p/oauth-signpost/) ([Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt))

Thanks go to all of the authors of these great little helpers!


[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/bzeiss/qs4j/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

